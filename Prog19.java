/*Fecha: 19-08-2020
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com */

/*Realice un algoritmo para determinar qué cantidad de dinero hay en un monedero, considerando que se tienen monedas de diez, cinco y un peso, y billetes de diez, veinte y cincuenta pesos.*/

import java.util.Scanner;
public class Prog19 {/*Inicio clase*/
	public static void main(String[] args){/*Inicio metodo principal*/
		/*Declaracion de variables*/
		int convertidor=0;
		double resultado=0;
		double monedero[ ]={ 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1, .50, .20, .10 };
		Scanner teclado = new Scanner (System.in);
		
		for (int i=0;i<=12;i++){
			System.out.print("Ingrese la cantidad de $"+ monedero[i] + " pesos que tiene: ");
			convertidor = teclado.nextInt();
			resultado=resultado+(convertidor*monedero[i]);

		}/*Fin for 1*/
		System.out.print("El total que contiene el monedero es de: $"+ resultado + " pesos");
	}/*Fin etodo principal*/
}/*Fin clase*/
