/*
 *Fecha: 21-08-2020 
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com*/

/*Realice un algoritmo para leer las calificaciones de N alumnos y de­termine el número de aprobados y reprobados.*/

import java.util.Scanner;
public class Prog23 {/*Inicio clase*/
	public static void main(String[] args){/*Inicio clase*/
		int numeroAlumnos;
		int i;
		int calificacion;
		Scanner teclado = new Scanner (System.in);

		System.out.print("Ingrese la cantidad de alumno: ");
		numeroAlumnos = teclado.nextInt();

		for(i=1; i<=numeroAlumnos; i++){/*Inicio for 1*/
			System.out.print("Ingrese la calificacion del alunmo: ");
			calificacion = teclado.nextInt();

			if(calificacion>=6){
				System.out.println("Alumno aprobado :D");
			}
			else{
				System.out.println("Alumno no aprobado :(");
			}
		}/*Fin for 1*/
	}/*Fin clase*/
}/*Fin clase*/
