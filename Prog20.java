/*Fecha: 19-08-2020
 * Autor:Fatima Azucena MC
 * fatimaazucenamartinez274@gmail.com */

/*Se requiere determinar el tiempo que tarda una persona en llegar de una ciudad a otra en bicicleta, considerando que lleva una velocidad constante.*/

import java.util.Scanner;
public class Prog20 {/*Inicio clase*/
	public static void main(String[] args){/*Inicio metodo principal*/
		/*Declaracion de variables*/
		float tiempoLlegada;
		float kRecorridos;
		float kHora;
		Scanner teclado = new Scanner (System.in);

		System.out.print("Ingrese los kilometros recorridos: ");
		kRecorridos = teclado.nextInt();
		System.out.print("Ingrese los K/H: ");
		kHora = teclado.nextInt();
		
		tiempoLlegada=(kRecorridos/kHora);

		System.out.print("El tiempo que tarda en llegar la persona es de: "+ tiempoLlegada +" hrs");

	}/*Fin de metodo principal*/
}/*Fin clase*/
