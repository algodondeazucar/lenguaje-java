/*
 *Fecha: 04_09_2020
 *Autor:Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com */

/*21Un vendedor ha realizado N ventas y desea saber cuántas fueron por 10,000 o menos, cuántas fueron por más de 10,000 pero por menos de 20,000, y cuánto fue el monto de las ventas de cada una y el monto global. Realice un algoritmo para determinar los totales. Represente la solución mediante diagrama de flujo, pseudocódigo.*/

import java.util.Scanner;

public class Prog21{/*Inicio clase*/
   public static void main(String[] args){/*Inicio metodo principal*/
	   /*Declaracion de variables*/
	   int numVentas;
	   int precioVenta;
	   int montoGlobal=0;
	   int montoMADE=0;
	   int montoMEDE=0;
	   int p;
	   int v;
	   Scanner teclado = new Scanner (System.in);

	   /*Ingresar datos por teclado*/
           System.out.print("Ingrese numero de ventas: ");
	   numVentas  = teclado.nextInt();
	   int g[] = new int [numVentas];
	   
	   for(p=0;p<numVentas;p++){/*Inicio for 1*/
		System.out.print("Ingrese el precio de cada venta: ");
	        g[p] = teclado.nextInt();
	        montoGlobal = montoGlobal + g[p];	
	  
		   /*Inicio condicional if-else-if*/
		   if(g[p]<=10000){
		      montoMEDE=montoMEDE+g[p];
		   }
		    else if ((g[p]>=10000)&&(g[p]<20000))
		   {
		       montoMADE=montoMADE+g[p];
		   }
	   }/*Fin for 2*/
	   System.out.print("El monto global es de: "+montoGlobal+" pesos"+"\n");
   }/*Fin metodo principal*/
}/*Fin clase*/
