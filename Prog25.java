/*
 *Fecha: 11_09_2020
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com */

/*25-Realice un algoritmo que determine el sueldo semanal de N trabajadores 
considerando que se les descuenta 5% de su sueldo si ganan entre 0 y 150 pesos.
Se les descuenta 7% si ganan más de 150 pero menos de 300, y 9% si ganan más de 
300 pero menos de 450. Los datos son horas trabajadas, sueldo por hora y nombre 
de cada trabajador. Represéntelo mediante diagrama de flujo, pseudocódigo.*/

import java.util.Scanner;

public class Prog25 {/*Inicio clase*/
	public static void main(String[] args){/*Inicio metodo principal*/
                /*Declaracion de variables*/
                Scanner teclado = new Scanner (System.in);
                double sueldoHoras;
                int canTrabajadores;
                string nombre;
                double dineroGanado;
                double sueldoBruto;
                double sueldoNeto;
                int i;

                /*Entrada de datos por teclado*/
                System.out.print("Inserte el precio por hora: ");
                sueldoHoras = teclado.nextInt();
                System.out.print("Inserte el numero de trabajadores: ");
                canTrabajadores = teclado.nextInt();

                for(i=1;i<=canTrabajadores;i++){/*Inicio for_1*/
                        System.out.print("Inserte el nombre del trabajador: ");
                        nombre = teclado.nextInt();
                        System.out.print("Inserte las horas trabajadas: ");
                        hTrabajadas = teclado.nextInt();
                        System.out.print("Inserte el dinero ganado: ");
                        dineroGanado = teclado.nextInt();


                        if((dineroGanado<0)&&(dineroGanado>=150)){/*Inicio if-else-if_1*/
                                sueldoBruto=40*(dineroGanado/hTrabajadas);
                                sueldoNeto=sueldoNeto-(sueldoNeto*0.05);
                        }
                         else if((dineroGanado>=150)&&(dineroGanado<=300)){
                                 sueldoBruto=40*(dineroGanado/hTrabajadas);
                                 sueldoNeto=sueldoNeto-(sueldoNeto*0.07);
                         }
                          else if((dineroGanado>300)&&(dineroGanado<=450)){
                                  sueldoBruto=40*(dineroGanado/hTrabajadas);
                                  sueldoNeto=sueldoNeto-(sueldoNeto*0.09);
                          }/*Fin if-else-if*/

			  System.out.print("El sueldo bruto de " + nombre + " es de: $"+ sueldoBruto + " pesos");
			  System.out.print("El sueldo neto de " + nombre + " es de: $" + sueldoNeto + " pesos");
                }/*Fin for_1*/
        }/*Fin metodo principal*/
}/*Inicio clase*/
