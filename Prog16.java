/*Fecha: 17-08-2020
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com*/
/*Realice un algoritmo que, con base en una calificación proporciona­da (0-10), indique con letra la calificación que le corresponde: 10 es “A”, 9 es “B”, 8 es “C”, 7 y 6 son “D”, y de 5 a 0 son “F”. Represente el diagrama de flujo, el pseudocódigo correspondiente.*/
import java.util.Scanner;
public class Prog16 {/*Inicio clase*/
	public static void main(String[] args){/*Inicio metodo principal*/
	        /*Delaracion de variables*/
		int opcion;
		Scanner teclado = new Scanner (System.in);
		System.out.print("Ingrese su calificacion: ");
		opcion = teclado.nextInt();

		switch (opcion){/*Inicio switch case*/
			case 10:
				System.out.print("Sacaste A :D");
			break;
			case 9:
				System.out.print("Sacaste B :D");
			break;
			case 8: 
				System.out.print("Sacaste C :)");
			break;
			case 7:
				System.out.print("Sacaste D :|");
			break;
			case 6:
				System.out.print("Sacaste D :|");
			break;
			case 5:
				System.out.print("Sacaste F :(");
			break;
			case 4:
				System.out.print("Sacaste F :|");
			break;
			case 3:
				System.out.print("Sacaste F :|");
			break;
			case 2:
				System.out.print("Sacaste F :|");
			break;
			case 1:
				System.out.print("Sacaste F :|");
			break;
			default:
			 	System.out.print("Opcion no valida");
			break;
		}/*Fin switch case*/
	}/*Fin metodo principal*/
}/*Fin clase*/
