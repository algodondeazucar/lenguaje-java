/*
 *Fecha: 25-08-2020
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com*/

/*La CONAGUA requiere determinar el pago que debe realizar una persona por el total de metros cúbicos que consume de agua.*/

import java.util.Scanner;

public class  Prog24{/*Inicio clase*/
 public static void main(String[] args){/*Inicio metodo principal*/
	/*Declaracion de variables*/
	int precioMetros;
	int metros;
	int resultado;
	Scanner teclado = new Scanner (System.in);

	System.out.print("Ingrese el precio por metro cuadrado: ");
	precioMetros = teclado.nextInt();
	System.out.print("Ingrese los metros cubicos de agua consumida: ");
	metros = teclado.nextInt();

	resultado=(precioMetros*metros);

	System.out.print("El total a pagar por los "+ metros+" cubicos es de: $"+ resultado +" pesos");
 }/*Fin metodo principal*/
}/*Fin clase*/
